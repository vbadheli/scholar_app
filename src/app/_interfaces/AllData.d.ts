﻿export interface AllData {
  data?: (DataEntity)[] | null;
}
export interface DataEntity {
  id: number;
  subject_id: number;
  title: string;
  created_at: string;
  updated_at: string;
  subjects: Subjects;
}
export interface Subjects {
  id: number;
  title: string;
  created_at: string;
  updated_at: string;
}
