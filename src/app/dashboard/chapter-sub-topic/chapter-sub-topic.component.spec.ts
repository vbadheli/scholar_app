import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterSubTopicComponent } from './chapter-sub-topic.component';

describe('ChapterSubTopicComponent', () => {
  let component: ChapterSubTopicComponent;
  let fixture: ComponentFixture<ChapterSubTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapterSubTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterSubTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
