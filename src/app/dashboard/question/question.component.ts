import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../_services/my-app.service';
import { AppComponent } from 'src/app/app.component';
import { AllData } from 'src/app/_interfaces/AllData';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddSubjectModalComponent } from '../modals/add-subject-modal/add-subject-modal.component';
import { AddChapterModalComponent } from '../modals/add-chapter-modal/add-chapter-modal.component';
import { AddTopicModalComponent } from '../modals/add-topic-modal/add-topic-modal.component';
import { AddSubTopicModalComponent } from '../modals/add-sub-topic-modal/add-sub-topic-modal.component';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import * as ClassicEditor from 'ckeditor5-build-classic-math';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  selectedSimpleItem;
  type = "chapter";
  parentType = "subject";
  myform: FormGroup;
  loading = false;
  submitted = false;
  isupdate = false;
  editMyData:any;
  error = false;
  errorMessage = "";
  // paginationData:any;
  alllevels;
  allquestions= [];
  allanswers= [];
  allsubjects= [];
  allchapters;
  alltopics;
  allsubtopics;
  selectedSubject;
  selectedChapter;
  selectedTopic;
  selectedSubTopic;
  public editor = ClassicEditor;
  columnDefs = [
    {headerName: 'Question', field: 'question', sortable: true,filter: true },
    {headerName: 'Level', field: 'level_id', sortable: true,filter: true},
    {headerName: 'Subject', field: 'subject.title', sortable: true, filter: true}
];

rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
];

  constructor(private formBuilder: FormBuilder,
    public service: MyAppService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router) { 
    }
  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      levelid: ['', Validators.required],
      subjectid: ['', Validators.required],
      chapterid: ['', Validators.required],
      topicid: ['', Validators.required],
      subtopicid: ['', Validators.required],
      question: ['', Validators.required],
      question_image: [''],
      option_a: ['', Validators.required],
      option_b: ['', Validators.required],
      option_c: ['', Validators.required],
      option_d: ['', Validators.required],
      option_images: [''],
      answerid: ['', Validators.required],
      solution: ['', Validators.required],
      solution_image: [''],
    });
    this.alllevels = [{title:'Hard',id: 1},{title:'Medium',id: 2}, {title:'Easy',id: 3}];
    this.allanswers = [{title:'A',id: 1},{title:'B',id: 2}, {title:'C',id: 3}, {title:'D',id: 4}];
    this.getQuestionData();
    this.getSubjects();
      
  }

  getQuestionData()
  {
    this.service.getAllData("question", 0).subscribe( (resp) => {
      this.allquestions = resp.data;
      console.log(this.allquestions);
    }, (err) => {

    })
  }
  get f() { return this.myform.controls; }

  onSubmit()
  {
    this.submitted = true;
    this.loading = true;
    this.error = false;
    this.errorMessage = "";
    if (this.myform.invalid) {
      this.loading = false;
      return;
    }

    
    let data = {
      "type": "question",
      "subject_id": this.f.subjectid.value,
      "chapter_id": this.f.chapterid.value,
      "chapter_topic_id": this.f.topicid.value,
      "chapter_sub_topic_id": this.f.subtopicid.value,
      "level_id": this.f.levelid.value,
      "question": this.f.question.value,
      "option_a": this.f.option_a.value,
      "option_b": this.f.option_b.value,
      "option_c": this.f.option_c.value,
      "option_d": this.f.option_d.value,
      "answer": this.f.answerid.value,
      "solution": this.f.solution.value,
  
  }
    this.service.addData(data).subscribe( (resp) => {
      this.resetForm();
      this.getQuestionData();
    }, (err) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = "Something went wrong";
    })
    
  }

  /*getAllSubjects(pageno)
  {
    let data = "subject";
    this.service.getAllData(data, pageno).subscribe( (resp) => {
      this.paginationData = resp.data;
      this.allSubjects = resp.data.data;
    }, (err) => {

    })
  }*/

  resetForm()
  {
    this.submitted = false;
    this.isupdate = false;
    this.loading = false;
    this.error = false;
    this.errorMessage = "";
    // this.myform.reset();
    this.f.question.setValue("");
    this.f.option_a.setValue("");
    this.f.option_b.setValue("");
    this.f.option_c.setValue("");
    this.f.option_d.setValue("");
    this.f.answerid.setValue("");
    this.f.solution.setValue("");

    this.editMyData={};
  }


  editData(id)
  {
    this.service.getData(this.type, id).subscribe( (resp) => {
      // this.paginationData = resp.data;
      this.isupdate = true;
      this.editMyData = resp.data;
      this.myform.controls['subjectid'].setValue(this.editMyData.subject_id);
      this.myform.controls['chaptername'].setValue(this.editMyData.title);
    }, (err) => {

    })
  }

  updateData()
  {
    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "title": this.f.chaptername.value,
      "id": this.editMyData.id
    }
    this.service.updateData(data).subscribe( (resp) => {
      this.resetForm();
      // this.paginationData = resp.data;
      // this.allD = resp.data.data;
    }, (err) => {

    })
  }

  deleteData(id)
  {
    let data = {
      "type": this.type,
      "id": id
    }

    this.service.deleteData(data).subscribe( (resp) => {
      this.resetForm();
    }, (err) => {

    })
  }

  getSubjects()
  {
    this.allsubjects = [];
    this.allchapters = [];
    this.alltopics = [];
    this.allsubtopics = [];
    this.f.subjectid.setValue("");
    this.f.chapterid.setValue("");
    this.f.topicid.setValue("");
    this.f.subtopicid.setValue("");

    this.service.getAllData("subject", 0).subscribe( (resp) => {
      this.allsubjects = resp.data;
    }, (err) => {

    })
  }
  getSubjectChapters(event)
  {
    this.allchapters = [];
    this.alltopics = [];
    this.allsubtopics = [];
    this.f.chapterid.setValue("");
    this.f.topicid.setValue("");
    this.f.subtopicid.setValue("");
    this.service.getSubjChapData(event.id).subscribe( (resp) => {
      this.allchapters = resp.data;
    }, (err) => {

    })
  }

  getChapterTopics(event)
  {
    this.alltopics = [];
    this.allsubtopics = [];
    this.f.topicid.setValue("");
    this.f.subtopicid.setValue("");
    this.service.getChapTopicData(event.id).subscribe( (resp) => {
      this.alltopics = resp.data;
    }, (err) => {

    })
  }

  getSubTopics(event)
  {
    this.allsubtopics = [];
    this.f.subtopicid.setValue("");
    this.service.getTopicSubTopicData(event.id).subscribe( (resp) => {
      this.allsubtopics = resp.data;
    }, (err) => {

    })
  }

  showAddNewModal(type)
  {
    if(type == "subject")
    {
      this.openSubjectModal();
    }
    else if(type == "chapter")
    {
      this.openChapterModal();
    }
    else if(type == "topic")
    {
      this.openTopicModal();
    }
    else if(type == "subtopic")
    {
      this.openSubTopicModal();
    }
  }

  openSubjectModal() {
    const modalRef = this.modalService.open(AddSubjectModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getSubjects();
      // this.allsubjects.push(data.data);
      this.f.subjectid.setValue(data.data.id);
      // console.log(this.f.subjectid.value);
      // this.f.
      //this.getAllData(this.type,1);
    }, (reason) => {
      // on dismiss
    });
  }

  openChapterModal(){
    const modalRef = this.modalService.open(AddChapterModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getSubjects();
      this.allsubjects.push(data.data);
      this.f.subjectid.setValue(data.data.id);
      // console.log(this.f.subjectid.value);
      // this.f.
      //this.getAllData(this.type,1);
    }, (reason) => {
      // on dismiss
    });
  }
  openTopicModal(){
    const modalRef = this.modalService.open(AddTopicModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getSubjects();
      // this.allsubjects.push(data.data);
      this.f.subjectid.setValue(data.data.id);
      // console.log(this.f.subjectid.value);
      // this.f.
      //this.getAllData(this.type,1);
    }, (reason) => {
      // on dismiss
    });
  }
  openSubTopicModal(){
    const modalRef = this.modalService.open(AddSubTopicModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getSubjects();
      // this.allsubjects.push(data.data);
      this.f.subjectid.setValue(data.data.id);
      // console.log(this.f.subjectid.value);
      // this.f.
      //this.getAllData(this.type,1);
    }, (reason) => {
      // on dismiss
    });
  }
}
