import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../_services/my-app.service';
import { AppComponent } from 'src/app/app.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddSubjectModalComponent } from '../modals/add-subject-modal/add-subject-modal.component';
import { ChooseQuestionsModalComponent } from '../modals/setpaper/choose-questions-modal/choose-questions-modal.component';

@Component({
  selector: 'app-set-paper',
  templateUrl: './set-paper.component.html',
  styleUrls: ['./set-paper.component.css']
})
export class SetPaperComponent extends AppComponent implements OnInit {

  allSubjectData;
  allChapterData;
  type = "subtopic";
  parentType = "topic";
  myform: FormGroup;
  loading = false;
  submitted = false;
  isupdate = false;
  editMyData:any;
  error = false;
  errorMessage = "";
  // paginationData:any;
  allsubjects;
  papertypes=["Full Paper","Chapterwise","Topicwise", "Sub-Topicwise","Groupwise"]
  allchapters;
  alltopics;
  allsubtopics;
  alllevels;
  constructor(private formBuilder: FormBuilder,
    public service: MyAppService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router) { 
      super(service);
    }

  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      papertype: ['', Validators.required],
      subjectid: ['', Validators.required],
      chapterid: ['', Validators.required],
      topicid: ['', Validators.required],
      subtopicname: ['', Validators.required],
      noofquestions: ['', Validators.required]
    });
    
    this.getSubjects();
  }

  getSubjects()
  {
    this.allsubjects = [];
    this.f.subjectid.setValue("");

    this.service.getAllData("subject", 0).subscribe( (resp) => {
      this.allsubjects = resp.data;
    }, (err) => {

    })
  }

  get f() { return this.myform.controls; }

  onSubmit()
  {
    this.submitted = true;
    this.loading = true;
    this.error = false;
    this.errorMessage = "";
    if (this.myform.invalid) {
      this.loading = false;
      return;
    }

    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "chapter_id": this.f.chapterid.value,
      "topic_id": this.f.topicid.value,
      "title": this.f.subtopicname.value,
      "noofquestions": this.f.noofquestions.value
    }
    this.service.addData(data).subscribe( (resp) => {
      this.resetForm();
    }, (err) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = "Something went wrong";
    })
    
  }

  /*getAllSubjects(pageno)
  {
    let data = "subject";
    this.service.getAllData(data, pageno).subscribe( (resp) => {
      this.paginationData = resp.data;
      this.allSubjects = resp.data.data;
    }, (err) => {

    })
  }*/

  resetForm()
  {
    this.submitted = false;
    this.isupdate = false;
    this.loading = false;
    this.error = false;
    this.errorMessage = "";
    this.myform.reset();
    this.editMyData={};
    this.getAllData(this.type,1);
  }


  editData(id)
  {
    this.service.getData(this.type, id).subscribe( (resp) => {
      // this.paginationData = resp.data;
      this.isupdate = true;
      this.editMyData = resp.data;
      this.myform.controls['subjectid'].setValue(this.editMyData.subject_id);
      this.myform.controls['chapterid'].setValue(this.editMyData.chapter_id);
      this.myform.controls['topicid'].setValue(this.editMyData.topic_id);
      this.myform.controls['subtopicname'].setValue(this.editMyData.title);
    }, (err) => {

    })
  }

  updateData()
  {
    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "chapter_id": this.f.chapterid.value,
      "topic_id": this.f.topicid.value,
      "title": this.f.subtopicname.value,
      "id": this.editMyData.id
    }
    this.service.updateData(data).subscribe( (resp) => {
      this.getAllData(this.type, 1);
      this.resetForm();
      // this.paginationData = resp.data;
      // this.allD = resp.data.data;
    }, (err) => {

    })
  }

  deleteData(id)
  {
    let data = {
      "type": this.type,
      "id": id
    }

    this.service.deleteData(data).subscribe( (resp) => {
      this.getAllData(this.type, 1);
      this.resetForm();
    }, (err) => {

    })
  }

  showAddNewModal(type)
  {
    if(type == "subject")
    {
      this.openSubjectModal();
    }
    // else if(type == "chapter")
    // {
    //   this.openChapterModal();
    // }
    // else if(type == "topic")
    // {
    //   this.openTopicModal();
    // }
    // else if(type == "subtopic")
    // {
    //   this.openSubTopicModal();
    // }
  }

  openSubjectModal() {
    const modalRef = this.modalService.open(AddSubjectModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getSubjects();
      this.f.subjectid.setValue(data.data.id);
    }, (reason) => {
      // on dismiss
    });
  }
  allChapData=[];
  getChaptersData()
  {
    this.service.getChaptersData(this.f.subjectid.value).subscribe( (resp) => {
      console.log(resp);
      this.allChapData = resp.data;

    }, (err) => {
      console.log(err);
    })
  }

  selectQuesManual(chid)
  {
    this.openChooseQuestionsModal(chid);
  }
  removeSelectedQues(chid)
  {

  }

  getSubjectChapters()
  {
    this.allchapters = [];
    this.f.chapterid.setValue("");
    this.service.getSubjChapData(this.f.subjectid.value).subscribe( (resp) => {
      this.allchapters = resp.data;
    }, (err) => {

    })
  }

  openChooseQuestionsModal(chid) {
    const modalRef = this.modalService.open(ChooseQuestionsModalComponent, {size: 'xl',scrollable: true, centered: true});
    modalRef.componentInstance.chapter_id = chid;
    modalRef.componentInstance.topic_id = 1;
    modalRef.componentInstance.questionsCount = 10;
    modalRef.result.then((data) => {
      // this.getSubjects();
      // this.f.subjectid.setValue(data.data.id);
    }, (reason) => {
      // on dismiss
    });
  }
}
