import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPaperComponent } from './set-paper.component';

describe('SetPaperComponent', () => {
  let component: SetPaperComponent;
  let fixture: ComponentFixture<SetPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
