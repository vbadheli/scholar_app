import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPapersComponent } from './list-papers.component';

describe('ListPapersComponent', () => {
  let component: ListPapersComponent;
  let fixture: ComponentFixture<ListPapersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPapersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPapersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
