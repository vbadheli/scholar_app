import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../_services/my-app.service';
import { AppComponent } from 'src/app/app.component';
import { AllData } from 'src/app/_interfaces/AllData';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddChapterModalComponent } from '../modals/add-chapter-modal/add-chapter-modal.component';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent extends AppComponent implements OnInit {

  type = "chapter";
  parentType = "subject";
  myform: FormGroup;
  loading = false;
  submitted = false;
  isupdate = false;
  editMyData:any;
  error = false;
  errorMessage = "";
  // paginationData:any;
  allsubjects;

  constructor(private formBuilder: FormBuilder,
    public service: MyAppService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router) { 
      super(service);
    }
    
  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      subjectid: ['', Validators.required],
      chaptername: ['', Validators.required]
    });
    // this.getAllParentData(this.parentType, 0);
    // this.allsubjects = this.service.getAllData(this.parentType, 0);
    this.service.getAllData(this.parentType, 0).subscribe( (resp) => {
      // this.paginationData = resp.data;
      this.allsubjects = resp.data;
    }, (err) => {

    })
      
    this.getAllData(this.type, 1);
  }

  get f() { return this.myform.controls; }

  onSubmit()
  {
    this.submitted = true;
    this.loading = true;
    this.error = false;
    this.errorMessage = "";
    if (this.myform.invalid) {
      this.loading = false;
      return;
    }

    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "title": this.f.chaptername.value
    }
    this.service.addData(data).subscribe( (resp) => {
      this.resetForm();
    }, (err) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = "Something went wrong";
    })
    
  }

  /*getAllSubjects(pageno)
  {
    let data = "chapter";
    this.service.getAllData(data, pageno).subscribe( (resp) => {
      this.paginationData = resp.data;
      this.allSubjects = resp.data.data;
    }, (err) => {

    })
  }*/

  resetForm()
  {
    this.submitted = false;
    this.isupdate = false;
    this.loading = false;
    this.error = false;
    this.errorMessage = "";
    this.myform.reset();
    this.editMyData={};
    this.getAllData(this.type,1);
  }


  editData(id)
  {
    this.service.getData(this.type, id).subscribe( (resp) => {
      // this.paginationData = resp.data;
      this.isupdate = true;
      this.editMyData = resp.data;
      this.myform.controls['subjectid'].setValue(this.editMyData.subject_id);
      this.myform.controls['chaptername'].setValue(this.editMyData.title);
    }, (err) => {

    })
  }

  updateData()
  {
    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "title": this.f.chaptername.value,
      "id": this.editMyData.id
    }
    this.service.updateData(data).subscribe( (resp) => {
      this.getAllData(this.type, 1);
      this.resetForm();
      // this.paginationData = resp.data;
      // this.allD = resp.data.data;
    }, (err) => {

    })
  }

  deleteData(id)
  {
    let data = {
      "type": this.type,
      "id": id
    }

    this.service.deleteData(data).subscribe( (resp) => {
      this.getAllData(this.type, 1);
      this.resetForm();
    }, (err) => {

    })
  }

  open() {
    const modalRef = this.modalService.open(AddChapterModalComponent);
    modalRef.componentInstance.name = 'World';
    modalRef.result.then((data) => {
      this.getAllData(this.type,1);
    }, (reason) => {
      // on dismiss
    });
  }


  

}
