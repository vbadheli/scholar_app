import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../../_services/my-app.service';

@Component({
  selector: 'app-add-subject-modal',
  templateUrl: './add-subject-modal.component.html',
  styleUrls: ['./add-subject-modal.component.css']
})
export class AddSubjectModalComponent implements OnInit {
  @Input() name;
  myform: FormGroup;
  loading = false;
  submitted = false;
  type="subject";
  error = false;
  errorMessage = "";

  constructor(private formBuilder: FormBuilder,
    public service: MyAppService, public activeModal: NgbActiveModal) {}
  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      subjectname: ['', Validators.required]
    });

  }

  get f() { return this.myform.controls; }

  onSubmit()
  {
    this.submitted = true;
    this.loading = true;
    this.error = false;
    this.errorMessage = "";

    if (this.myform.invalid) {
      this.loading = false;
      return;
    }
    let data = {
      "type": this.type,
      "title": this.f.subjectname.value
    }

    this.service.addData(data).subscribe( (resp) => {
      this.activeModal.close(resp);
    }, (err) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = "Something went wrong";
    })
    
  }

  
}
