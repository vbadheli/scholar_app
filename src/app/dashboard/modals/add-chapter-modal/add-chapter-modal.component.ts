import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../../_services/my-app.service';

@Component({
  selector: 'app-add-chapter-modal',
  templateUrl: './add-chapter-modal.component.html',
  styleUrls: ['./add-chapter-modal.component.css']
})
export class AddChapterModalComponent implements OnInit {

  loading = false;
  submitted = false;
  isupdate = false;
  editMyData:any;
  error = false;
  errorMessage = "";
  myform: FormGroup;

  type = "chapter";
  parentType = "subject";
  allsubjects;

  constructor(private formBuilder: FormBuilder,
    public service: MyAppService, 
    public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      subjectid: ['', Validators.required],
      chaptername: ['', Validators.required]
    });

    this.service.getAllData(this.parentType, 0).subscribe( (resp) => {
      this.allsubjects = resp.data;
    }, (err) => {

    })
  }

  get f() { return this.myform.controls; }
  
  onSubmit()
  {
    this.submitted = true;
    this.loading = true;
    this.error = false;
    this.errorMessage = "";
    if (this.myform.invalid) {
      this.loading = false;
      return;
    }

    let data = {
      "type": this.type,
      "subject_id": this.f.subjectid.value,
      "title": this.f.chaptername.value
    }
    this.service.addData(data).subscribe( (resp) => {
      this.activeModal.close();
    }, (err) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = "Something went wrong";
    })
    
  }
}
