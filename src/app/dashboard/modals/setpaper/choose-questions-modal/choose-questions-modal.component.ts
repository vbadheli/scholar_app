import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MyAppService } from './../../../../_services/my-app.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-choose-questions-modal',
  templateUrl: './choose-questions-modal.component.html',
  styleUrls: ['./choose-questions-modal.component.css']
})
export class ChooseQuestionsModalComponent implements OnInit {

  questionsCount;
  topic_id;
  chapter_id;
  loading;
  error;
  errorMessage;
  alltopics = [];
  myform: FormGroup;
  allQuestionsData=[];
  submitted;

  constructor(private formBuilder: FormBuilder,
    private service: MyAppService,
    public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.myform = this.formBuilder.group({
      topicid: ['', Validators.required],
      questionscount: ['', Validators.required]
    });
    this.f.topicid.setValue(this.topic_id);
    this.getTopicQuestions();
    this.getChapterTopics();
  }
  get f() { return this.myform.controls; }

  getChapterTopics()
  {
    this.alltopics = [];
    this.f.topicid.setValue("");
    this.service.getChapTopicData(this.chapter_id).subscribe( (resp) => {
      this.alltopics = resp.data;
    }, (err) => {

    })
  }

  getTopicQuestions()
  {
    this.allQuestionsData = [];
    this.service.getTopicQuetionsData(this.f.topicid.value).subscribe( (resp) => {
      this.allQuestionsData = resp.data;
    }, (err) => {

    })
  }

  onSubmit()
  {
    
  }

}
