import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseQuestionsModalComponent } from './choose-questions-modal.component';

describe('ChooseQuestionsModalComponent', () => {
  let component: ChooseQuestionsModalComponent;
  let fixture: ComponentFixture<ChooseQuestionsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseQuestionsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseQuestionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
