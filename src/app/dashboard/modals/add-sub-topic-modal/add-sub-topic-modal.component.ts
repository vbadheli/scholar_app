import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-sub-topic-modal',
  templateUrl: './add-sub-topic-modal.component.html',
  styleUrls: ['./add-sub-topic-modal.component.css']
})
export class AddSubTopicModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

}
