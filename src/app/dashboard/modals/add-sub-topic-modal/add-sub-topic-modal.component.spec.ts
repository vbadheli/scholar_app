import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubTopicModalComponent } from './add-sub-topic-modal.component';

describe('AddSubTopicModalComponent', () => {
  let component: AddSubTopicModalComponent;
  let fixture: ComponentFixture<AddSubTopicModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubTopicModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubTopicModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
