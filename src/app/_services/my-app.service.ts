import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AllData } from '../_interfaces/AllData';

@Injectable({
  providedIn: 'root'
})
export class MyAppService {

  constructor(private http:HttpClient) { }

  addData(data)
  {
    return this.http.post<any>(`${environment.apiUrl}/add-data`, data);
  }

  getAllData(data, pageno) {
    return this.http.get<AllData>(`${environment.apiUrl}/get-all-data/${data}?page=${pageno}`);
  }

  getData(data, id) {
    return this.http.get<any>(`${environment.apiUrl}/get-data/${data}/${id}`);
  }

  deleteData(data) {
    return this.http.post<any>(`${environment.apiUrl}/delete-data`, data);
  }

  updateData(data) {
    return this.http.post<any>(`${environment.apiUrl}/update-data`, data);
  }


  getSubjChapData(subject_id) {
    return this.http.get<any>(`${environment.apiUrl}/get-subject-chapter-data/${subject_id}`);
  }
  getChapTopicData(chapter_id) {
    return this.http.get<any>(`${environment.apiUrl}/get-chapter-topic-data/${chapter_id}`);
  }
  getTopicSubTopicData(topic_id) {
    return this.http.get<any>(`${environment.apiUrl}/get-topic-subtopic-data/${topic_id}`);
  }

  getChaptersData(subject_id) {
    return this.http.get<any>(`${environment.apiUrl}/test/${subject_id}`);
  }

  getChapterTopicsData(chapter_id) {
    return this.http.get<any>(`${environment.apiUrl}/test2/${chapter_id}`);
  }
  getTopicQuetionsData(topic_id) {
    return this.http.get<any>(`${environment.apiUrl}/test3/${topic_id}`);
  }

}
