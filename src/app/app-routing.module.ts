import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectComponent } from './dashboard/subject/subject.component';
import { ChapterComponent } from './dashboard/chapter/chapter.component';
import { ChapterTopicComponent } from './dashboard/chapter-topic/chapter-topic.component';
import { ChapterSubTopicComponent } from './dashboard/chapter-sub-topic/chapter-sub-topic.component';
import { QuestionComponent } from './dashboard/question/question.component';
import { SetPaperComponent } from './dashboard/set-paper/set-paper.component';
import { ListPapersComponent } from './dashboard/list-papers/list-papers.component';

const routes: Routes = [
  {"path":"subjects", component: SubjectComponent},
  {"path":"chapters", component: ChapterComponent},
  {"path":"topics", component: ChapterTopicComponent},
  {"path":"sub-topics", component: ChapterSubTopicComponent},
  {"path":"questions", component: QuestionComponent},
  {"path":"set-paper", component: SetPaperComponent},
  {"path":"", component: ListPapersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
