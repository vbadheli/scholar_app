declare module namespace {

    export interface Subjects {
        id: number;
        title: string;
        created_at: Date;
        updated_at: Date;
    }

    export interface Datum {
        id: number;
        subject_id: number;
        title: string;
        created_at: Date;
        updated_at: Date;
        subjects: Subjects;
    }

    export interface Data {
        current_page: number;
        data: Datum[];
        first_page_url: string;
        from: number;
        last_page: number;
        last_page_url: string;
        next_page_url?: any;
        path: string;
        per_page: number;
        prev_page_url?: any;
        to: number;
        total: number;
    }

    export interface RootObject {
        data: Data;
    }

}

