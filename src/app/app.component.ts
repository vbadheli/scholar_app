import { Component } from '@angular/core';
import { MyAppService } from './_services/my-app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'scholar';
  paginationData:any;
  allParentData=[];
  allData=[];
  constructor(public service: MyAppService){

  }
  getAllData(type, pageno)
  {
    this.service.getAllData(type, pageno).subscribe( (resp) => {
      this.paginationData = resp.data;
      this.allData = this.paginationData.data;
    }, (err) => {

    })
  }

  getAllParentData(parentType, pageno)
  {
    this.service.getAllData(parentType, pageno).subscribe( (resp) => {
      // this.paginationData = resp.data;
      this.allParentData = resp.data;
    }, (err) => {

    })
  }

  getAllParentDataNew(parentType, pageno)
  {
    let data
    this.service.getAllData(parentType, pageno).subscribe( (resp) => {
      // this.paginationData = resp.data;
      data = resp.data;
    }, (err) => {

    })
    console.log(data);
  }
}
