import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './dashboard/navigation/navigation.component';
import { SidebarComponent } from './dashboard/sidebar/sidebar.component';
import { FooterComponent } from './dashboard/footer/footer.component';
import { DemoContentComponent } from './dashboard/demo-content/demo-content.component';
import { SubjectComponent } from './dashboard/subject/subject.component';
import { ChapterComponent } from './dashboard/chapter/chapter.component';
import { ChapterTopicComponent } from './dashboard/chapter-topic/chapter-topic.component';
import { ChapterSubTopicComponent } from './dashboard/chapter-sub-topic/chapter-sub-topic.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { NgbModule,NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { SetPaperComponent } from './dashboard/set-paper/set-paper.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuestionComponent } from './dashboard/question/question.component';
import { AgGridModule } from 'ag-grid-angular';
import { AddSubjectModalComponent } from './dashboard/modals/add-subject-modal/add-subject-modal.component';
import { AddChapterModalComponent } from './dashboard/modals/add-chapter-modal/add-chapter-modal.component';
import { AddTopicModalComponent } from './dashboard/modals/add-topic-modal/add-topic-modal.component';
import { AddSubTopicModalComponent } from './dashboard/modals/add-sub-topic-modal/add-sub-topic-modal.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ListPapersComponent } from './dashboard/list-papers/list-papers.component';
import { ChooseQuestionsModalComponent } from './dashboard/modals/setpaper/choose-questions-modal/choose-questions-modal.component';
// import { NgbdModalComponent, NgbdModalContent } from './modal-component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SidebarComponent,
    FooterComponent,
    DemoContentComponent,
    SubjectComponent,
    ChapterComponent,
    ChapterTopicComponent,
    ChapterSubTopicComponent,
    SetPaperComponent,
    QuestionComponent,
    AddSubjectModalComponent,
    AddChapterModalComponent,
    AddTopicModalComponent,
    AddSubTopicModalComponent,
    ListPapersComponent,
    ChooseQuestionsModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgbPaginationModule, 
    NgbAlertModule,
    NgSelectModule,
    CKEditorModule,
    AgGridModule.withComponents([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
